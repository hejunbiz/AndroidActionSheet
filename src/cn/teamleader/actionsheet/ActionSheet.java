package cn.teamleader.actionsheet;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class ActionSheet extends Activity implements OnClickListener {
	Context mContext;
	/**
	 * 照相View
	 */
	TextView takePictureView;
	/**
	 * 相册View
	 */
	TextView albumView;
	/**
	 * 取消View
	 */
	TextView cancelView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.action_sheet);
		this.mContext = this;

		takePictureView = (TextView) findViewById(R.id.take_picture);
		albumView = (TextView) findViewById(R.id.choose_album);
		cancelView = (TextView) findViewById(R.id.cancel);
		
		takePictureView.setOnClickListener(this);
		albumView.setOnClickListener(this);
		cancelView.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.take_picture:// 拍照
			Toast.makeText(mContext, "拍照", Toast.LENGTH_SHORT).show();
			break;
		case R.id.choose_album:// 读取相册
			Toast.makeText(mContext, "读取相册", Toast.LENGTH_SHORT).show();
			break;
		case R.id.cancel:// 取消
			Toast.makeText(mContext, "取消", Toast.LENGTH_SHORT).show();
			break;

		default:
			break;
		}
		finish();
		//关闭窗体动画显示  
	    overridePendingTransition(R.anim.activity_close,0);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		finish();
		//关闭窗体动画显示  
	    this.overridePendingTransition(R.anim.activity_close,0);
	}
}
